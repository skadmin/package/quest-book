<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Components\Front;

use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\Validators;
use Skadmin\QuestBook\BaseControl;
use Skadmin\QuestBook\Doctrine\QuestBook\QuestBookFacade;
use Skadmin\QuestBook\Mail\CMailQuestBookCreate;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class FormQuestBook extends FormControl
{
    use APackageControl;

    private QuestBookFacade $facade;
    private LoaderFactory   $webLoader;
    private ?string         $entity;
    private ?string         $entityId;

    public function __construct(?string $entity, ?string $entityId, QuestBookFacade $facade, Translator $translator, LoaderFactory $webLoader)
    {
        parent::__construct($translator);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->entity   = $entity;
        $this->entityId = $entityId;
    }

    public function getTitle() : string
    {
        return 'form.quest-book.front.create.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('reCaptchaInvisible'),
        ];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formQuestBook.latte'));

        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('sender', 'form.quest-book.front.create.sender')
            ->setRequired('form.quest-book.front.create.sender.req');
        $form->addText('recipient', 'form.quest-book.front.create.recipient')
            ->setRequired('form.quest-book.front.create.recipient.req');
        $form->addTextArea('content', 'form.quest-book.front.create.content', null, 10)
            ->setRequired('form.quest-book.front.create.content.req');

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // BUTTON
        $form->addSubmit('send', 'form.quest-book.front.create.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $this->facade->create($this->entity, $this->entityId, $values->sender, $values->recipient, $values->content);

        $this->onFlashmessage('form.quest-book.front.create.flash.success.create', Flash::SUCCESS);

        $this->onSuccess($form, $values, $form->isSubmitted()->name);

        $form->reset();
        $this->redrawControl('snipForm');
    }
}
