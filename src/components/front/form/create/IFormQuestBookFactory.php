<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Components\Front;

interface IFormQuestBookFactory
{
    public function create(?string $entity = null, ?string $entityId = null) : FormQuestBook;
}
