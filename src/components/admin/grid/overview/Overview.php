<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\SystemDir;
use App\Model\System\Utils;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\Application\Responses\FileResponse;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Skadmin\QuestBook\BaseControl;
use Skadmin\QuestBook\Doctrine\QuestBook\QuestBook;
use Skadmin\QuestBook\Doctrine\QuestBook\QuestBookFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Tracy\Debugger;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use function count;
use function implode;
use function intval;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    /** @persistent */
    public ?string          $identifier = null;
    public ?string          $entity     = null;
    public ?string          $entityId   = null;
    private QuestBookFacade $facade;
    private SystemDir       $systemDir;

    public function __construct(QuestBookFacade $facade, Translator $translator, User $user, SystemDir $systemDir)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;

        $this->systemDir = $systemDir;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        if ($this->getPresenter()->getParameter('entity') !== null) {
            $this->entity     = $this->getPresenter()->getParameter('entity');
            $this->entityId   = $this->getPresenter()->getParameter('entityId');
            $this->identifier = sprintf('%s::%s', $this->entity, $this->entityId);
            $this->isModal    = true;
        } elseif ($this->identifier !== null) {
            [$this->entity, $this->entityId] = explode('::', $this->identifier);
            $this->isModal = true;
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->drawBox = $this->drawBox;

        if ($this->getPresenterIfExists() && $this->identifier !== null) {
            $presenter        = $this->getPresenter();
            $template->isAjax = $presenter->isAjax();
        } else {
            $template->isAjax = false;
        }

        $template->render();
    }

    public function getTitle() : string
    {
        return 'quest-book.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        // DATA
        $dataEntities = [];
        foreach ($this->facade->getAvailableEntities() as $availableEntity) {
            $key                = sprintf('%s::%s', $availableEntity['entity'], $availableEntity['entityId']);
            $value              = sprintf('%s::%s', $this->translator->translate($availableEntity['entity']), $availableEntity['entityId']);
            $dataEntities[$key] = $value;
        }

        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        if ($this->entity !== null && $this->entityId !== null) {
            $grid->setDataSource($this->facade->getModelForEntity($this->entity, $this->entityId));
        } else {
            $grid->setDataSource($this->facade->getModel()->orderBy('a.id', 'DESC'));
        }

        // COLUMNS
        if ($this->identifier === null) {
            $grid->addColumnText('entity', 'grid.quest-book.overview.entity')
                ->setRenderer(fn(QuestBook $qb) : string => $dataEntities[sprintf('%s::%s', $qb->getEntity(), $qb->getEntityId())]);
        }
        $grid->addColumnText('sender', 'grid.quest-book.overview.sender');
        $grid->addColumnText('recipient', 'grid.quest-book.overview.recipient');
        $grid->addColumnText('content', 'grid.quest-book.overview.content')
            ->setRenderer(fn(QuestBook $qb) : string => Strings::truncate(strip_tags($qb->getContent()), 100));

        // FILTER
        $grid->addFilterSelect('entity', 'grid.quest-book.overview.entity', $dataEntities)
            ->setPrompt(Constant::PROMTP)
            ->setCondition(function (QueryBuilder $qb, $value) {
                [$entity, $entityId] = explode('::', $value);

                $criteria = Criteria::create();
                $criteria->andWhere(Criteria::expr()->andX(
                    Criteria::expr()->eq('entity', $entity),
                    Criteria::expr()->eq('entityId', $entityId),
                ));
                $qb->addCriteria($criteria);
            });
        $grid->addFilterText('sender', 'grid.quest-book.overview.sender');
        $grid->addFilterText('recipient', 'grid.quest-book.overview.recipient');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $grid->addActionCallback('remove', 'grid.quest-book.overview.action.remove', [$this, 'gridOverviewRemove'])
                ->setConfirmation(new StringConfirmation('grid.quest-book.overview.action.remove %%s', 'sender'))
                ->setClass('btn btn-xs btn-outline-danger ajax')
                ->setIcon('trash');
        }

        // DETAIL
        $detail = $grid->setItemsDetail(__DIR__ . '/detail.latte');

        $grid->addExportCallback('grid.quest-book.overview.export.txt', [$this, 'gridOverviewExportTxt'], true)
            ->setClass('btn btn-xs btn-outline-primary')
            ->setIcon('download');

        $grid->onRedraw[] = [$this, 'gridOverviewOnRedraw'];

        return $grid;
    }

    public function gridOverviewRemove(string $id) : void
    {
        $presenter = $this->getPresenterIfExists();

        if ($presenter !== null) {
            if ($this->facade->remove(intval($id))) {
                $presenter->flashMessage('grid.quest-book.overview.flash.remove.success', Flash::SUCCESS);
            } else {
                $presenter->flashMessage('grid.quest-book.overview.flash.remove.success', Flash::DANGER);
            }
        }

        $this['grid']->reload();
    }

    /**
     * @param array<QuestBook> $questBooks
     */
    public function gridOverviewExportTxt(array $questBooks) : void
    {
        $tempDir = $this->systemDir->getPathApp(['..', 'temp', 'txt']);
        $this->systemDir->createDir($tempDir);

        $tempFile = implode(DIRECTORY_SEPARATOR, [$tempDir, sprintf('%s-%s.txt', time(), Random::generate())]);

        $export = fopen($tempFile, 'w');

        foreach ($questBooks as $qb) {
            fwrite($export, sprintf("%s\n", strip_tags($qb->getContent())));
            fwrite($export, "---------------------------------------------------\n");
            fwrite($export, sprintf("pro: %s | autor: %s | přidáno: %s\n", $qb->getRecipient(), $qb->getSender(), $qb->getCreatedAt()->format('d.m.Y H:i')));
            fwrite($export, "\n");
            fwrite($export, "\n");
        }

        fclose($export);

        $this->getPresenter()->sendResponse(new FileResponse(
            $tempFile,
            $this->translator->translate(new SimpleTranslation('grid.quest-book.overview.export.txt.name %s', [date('Ymd-His')])) . '.txt'
        ));
    }

    public function gridOverviewOnRedraw() : void
    {
        if ($this->getPresenterIfExists() !== null) {
            $this->getPresenter()->redrawControl('snipModal', false);
            $this->getPresenter()->payload->dontHideModalbackdrop = true;
        }
    }
}
