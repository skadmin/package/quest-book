<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Doctrine\QuestBook;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;

final class QuestBookFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = QuestBook::class;
    }

    public function create(?string $entity, ?string $entityId, string $sender, string $recipient, string $content) : QuestBook
    {
        $questBook = $this->get();

        $questBook->create($entity, $entityId, $sender, $recipient, $content);

        $this->em->persist($questBook);
        $this->em->flush();

        return $questBook;
    }

    public function get(?int $id = null) : QuestBook
    {
        if ($id === null) {
            return new QuestBook();
        }

        $questBook = parent::get($id);

        if ($questBook === null) {
            return new QuestBook();
        }

        return $questBook;
    }

    public function getModelForEntity(?string $entity, ?string $entityId) : QueryBuilder
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('a.entity', $entity),
            Criteria::expr()->eq('a.entityId', $entityId)
        ));
        $criteria->orderBy(['a.id' => Criteria::DESC]);

        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function getCount(?QuestBookFilter $filter = null) : int
    {
        $qb = $this->findQb($filter);
        $qb->select('count(a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /** @return array<QuestBook> */
    public function find(?QuestBookFilter $filter = null, ?int $limit = null, ?int $offset = null) : array
    {
        $qb = $this->findQb($filter, $limit, $offset);

        return $qb->getQuery()
            ->getResult();
    }

    private function findQb(?QuestBookFilter $filter, ?int $limit = null, ?int $offset = null) : QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.id', 'DESC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        return $qb->addCriteria($criteria);
    }

    public function getAvailableEntities() : array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->select('a.entity, a.entityId')
            ->groupBy('a.entity, a.entityId');

        return $qb->getQuery()->getArrayResult();
    }

    public function remove(?int $id = null) : bool
    {
        $questBook = $this->get($id);

        if ($questBook->isLoaded()) {
            $this->em->remove($questBook);
            $this->em->flush();
            return true;
        }

        return false;
    }
}
