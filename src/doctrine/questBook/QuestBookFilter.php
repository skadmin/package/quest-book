<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Doctrine\QuestBook;

use SkadminUtils\DoctrineTraits\ACriteriaFilter;
use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use function trim;

final class QuestBookFilter extends ACriteriaFilter
{
    use SmartObject;

    private ?string $entity;
    private ?string $entityId;

    public function __construct(?string $entity = null, ?string $entityId = null)
    {
        $this->entity   = $entity;
        $this->entityId = $entityId;
    }

    public function getEntity() : ?string
    {
        return $this->entity;
    }

    public function setEntity(?string $entity) : void
    {
        $this->entity = $entity;
    }

    public function getEntityId() : ?string
    {
        return $this->entityId;
    }

    public function setEntityId(?string $entityId) : void
    {
        $this->entityId = $entityId;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a') : void
    {
        $expr = Criteria::expr();

        if ($this->getEntity() !== null) {
            $criteria->andWhere($expr->eq($this->getEntityName($alias, 'entity'), $this->getEntity()));
        }

        if ($this->getEntityId() !== null) {
            $criteria->andWhere($expr->eq($this->getEntityName($alias, 'entityId'), $this->getEntityId()));
        }
    }
}
