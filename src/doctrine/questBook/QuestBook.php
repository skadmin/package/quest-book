<?php

declare(strict_types=1);

namespace Skadmin\QuestBook\Doctrine\QuestBook;

use Nette\Utils\Validators;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class QuestBook
{
    use Entity\BaseEntity;
    use Entity\Content;

    #[ORM\Column]
    private string $sender = '';

    #[ORM\Column]
    private string $recipient = '';

    #[ORM\Column(nullable: true)]
    protected ?string $entity;

    #[ORM\Column(nullable: true)]
    protected ?string $entityId;

    public function create(?string $entity, ?string $entityId, string $sender, string $recipient, string $content) : void
    {
        $this->entity    = $entity;
        $this->entityId  = $entityId;
        $this->sender    = $sender;
        $this->recipient = $recipient;
        $this->content   = $content;
    }

    public function getSender() : string
    {
        return $this->sender;
    }

    public function getRecipient() : string
    {
        return $this->recipient;
    }

    public function getEntity() : ?string
    {
        return $this->entity;
    }

    public function getEntityId() : ?string
    {
        return $this->entityId;
    }

}
